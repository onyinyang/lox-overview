# Lox Overview

Lox is a privacy preserving, reputation-based bridge distribution system that is being integrated into Tor. This integration has multiple pieces that are divided into different repositories. For that reason, this project was created to act as a common space for overarching Lox designs, issues, questions, roadmaps, etc. 

